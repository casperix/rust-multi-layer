mod string_io;
mod pix_io;

use clap::Parser;
use std::io::{Error, Read, Write};
use crate::pix_io::{pack, unpack};


#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct PackArguments {
	#[arg(short, long)]
	albedo: String,

	#[arg(short, long)]
	normals: String,

	#[arg(short, long)]
	output: String,
}

fn main() -> Result<(), Error> {
	let args = PackArguments::try_parse().expect("Invalid arguments");

	pack(&args.albedo, &args.normals, &args.output).expect("TODO: panic message");

	return Ok(());
}

#[cfg(test)]
mod tests {
	use std::env;
	use super::*;

	#[test]
	fn test_pack() {
		println!("cur-dir: {:?}", &env::current_dir());

		let  args = PackArguments {
			albedo: String::from("playground/in/albedo.png"),
			normals: String::from("playground/in/normals.png"),
			output: String::from("playground/out/output.pix"),
		};

		pack(&args.albedo, &args.normals, &args.output).expect("TODO: panic message");

		unpack("playground/out/layer1.png", "playground/out/layer2.png", &args.output);

	}
}