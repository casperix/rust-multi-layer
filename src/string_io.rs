use std::fs::File;
use std::io::{BufReader, BufWriter, Read, Write};
use std::string::FromUtf8Error;

pub fn write_string(mut buf_writer: &mut BufWriter<File>, value: &str) {
	let bytes = value.as_bytes();

	let len = bytes.len() as u32;
	buf_writer.write(&len.to_be_bytes());
	buf_writer.write(bytes);
}

pub fn read_string(mut buf_reader: &mut BufReader<File>) -> Result<String, FromUtf8Error> {
	let mut buffer_length = [0; 4];
	buf_reader.read_exact(&mut buffer_length);
	let data_length = u32::from_be_bytes(buffer_length);

	let mut buffer_content = vec![0; data_length as usize];
	buf_reader.read_exact(&mut buffer_content);

	String::from_utf8(buffer_content)
}
