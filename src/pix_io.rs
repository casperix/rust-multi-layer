
use std::fs::File;
use std::io::{BufReader, BufWriter, Error, Read, Write};
use lebe::io::ReadPrimitive;

use image::{DynamicImage, EncodableLayout, GenericImageView, ImageError, RgbaImage};
use image::io::Reader as ImageReader;
use crate::string_io::{read_string, write_string};

const PIX_VERSION: &str = "0.1.0";

pub fn pack(layer1: &str, layer2: &str, output_name: &str) -> Result<(), ImageError> {

	let img1 = ImageReader::open(layer1)?.decode()?;
	let img2 = ImageReader::open(layer2)?.decode()?;

	let buffer1 = img1.to_rgba8();
	let buffer2 = img2.to_rgba8();

	let rgba1 = buffer1.as_raw();
	let rgba2 = buffer2.as_raw();

	let width = img1.width();
	let height = img1.height();
	let layers_amount = 2u32;

	let output = File::create(output_name)?;

	let mut writer = BufWriter::new(output);

	write_string(&mut writer, PIX_VERSION);

	writer.write(&width.to_be_bytes());
	writer.write(&height.to_be_bytes());
	writer.write(&layers_amount.to_be_bytes());

	writer.write(rgba1);
	writer.write(rgba2);

	writer.flush();

	Ok(())
}


pub fn unpack(layer1: &str, layer2: &str, source_name: &str) -> Result<(), Error> {
	let output = File::open(source_name)?;
	let mut reader = BufReader::new(output);

	let out_version = read_string(&mut reader).expect("cant read");

	assert_eq!(out_version, PIX_VERSION);

	let width = u32::read_from_big_endian(&mut reader)?;
	let height = u32::read_from_big_endian(&mut reader)?;
	let layers_amount = u32::read_from_big_endian(&mut reader)?;

	let mut img1 = DynamicImage::new_rgba8(width, height);
	let mut v1 = img1.as_mut_rgba8().expect("xm1");
	let mut b1 = v1.as_mut();

	let mut img2 = DynamicImage::new_rgba8(width, height);
	let mut v2 = img2.as_mut_rgba8().expect("xm2");
	let mut b2 = v2.as_raw();

	reader.read_exact(&mut b1);//тут вроде есть байтовый массив
	reader.read_exact(&mut v2);//А тут не успел ничего написать... Но оно как-то выдает правильный результат.

	img1.save(layer1);
	img2.save(layer2);

	Ok(())
}